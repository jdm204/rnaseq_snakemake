logfile <- file(snakemake@log[[1]], open="wt")
sink(logfile, type = "message")

message("Script: loading packages")

library(tidyverse)

if (exists("snakemake")) {
    ## snakemake is running this script
    deseq_results <- snakemake@input[[1]]
    gage_results <- snakemake@output[[1]]
} else {
    ## this script is interactive
    deseq_results <- "results/"
    gage_results <- snakemake@output[[1]]
}

## convert path-friendly representation of formula to formula
formula_obj <- formula_string %>%
    str_replace_all("BY", "~") %>%
    str_replace_all("_", " ") %>%
    formula()

samples <- sample_table_path %>%
    ## read in sample table
    read_tsv %>%
    ## don't need fastq info
    select(-c(pair, fastq)) %>%
    distinct %>%
    ## LR... are resistant cells, otherwise sensitive
    mutate(condition = if_else(startsWith(sample, "LR"), "LR", "Sensitive"),
           condition = relevel(as.factor(condition), ref="Sensitive"),
           names = paste(patient, sample, sep = "_")) %>%
    ## we don't want to vectorise str_interp 
    rowwise %>%
    ## paths to quant files
    mutate(files = str_interp("${q_prefix}/${patient}_${sample}/${q_suffix}"))

## main tximeta and DESeq functions
dds <- samples %>%
    ## transcript import with automatic metadata
    tximeta %>%
    ## assign transcripts to genes
    summarizeToGene %>%
    ## load into DESeq2, specifying experimental design
    DESeqDataSet(design = formula_obj) %>%
    ## fit model
    DESeq

## save deseq object
dir.create(output_deseq %>% dirname, showWarnings = FALSE)
save(dds, file=output_deseq)

## collect gene information from tximeta
gene_info <- tibble(id = rowData(dds)$gene_id,
                    symbol = rowData(dds)$symbol,
                    description = rowData(dds)$description)

## collect results and add gene info
results <- dds %>%
    results(tidy=TRUE) %>%
    rename(id=row) %>%
    inner_join(gene_info,
               by="id") %>%
    tibble %>%
    select(symbol, everything(), description, id) %>%
    arrange(padj)

## write out results table
results %>%
    write_tsv(output_results_table)
