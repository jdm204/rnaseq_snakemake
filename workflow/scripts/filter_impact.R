library(tidyverse)

indf <- snakemake@input[[1]]
outdf <- snakemake@output[[1]]

indf %>%
    read_tsv %>%
    filter(vep_IMPACT %in% c("MODERATE", "HIGH")) %>%
    write_tsv(outdf)
