library(tximeta)
library(DESeq2)
library(tidyverse)

## this script is interactive
q_prefix <- "results/salmon"
q_suffix <- "quant.sf"
formula_string <- "BY_condition"
sample_table_path <- "config/units.tsv"
output_deseq <- "results/deseq2/individual/"

## convert path-friendly representation of formula to formula
formula_obj <- formula_string %>%
    str_replace_all("BY", "~") %>%
    str_replace_all("_", " ") %>%
    formula()

samples <- sample_table_path %>%
    ## read in sample table
    read_tsv %>%
    ## don't need fastq info
    select(-c(pair, fastq)) %>%
    distinct %>%
    ## LR... are resistant cells, otherwise sensitive
    mutate(condition = if_else(startsWith(sample, "LR"), "LR", "Sensitive"),
           condition = relevel(as.factor(condition), ref="Sensitive"),
           names = paste(patient, sample, sep = "_")) %>%
    ## we don't want to vectorise str_interp 
    rowwise %>%
    ## paths to quant files
    mutate(files = str_interp("${q_prefix}/${patient}_${sample}/${q_suffix}")) %>%
    split(f=.$patient)

## main tximeta and DESeq functions
dds <- samples %>%
    map(function(sample) {
        sample %>%
            ## transcript import with automatic metadata
            tximeta %>%
            ## assign transcripts to genes
            summarizeToGene %>%
            ## load into DESeq2, specifying experimental design
            DESeqDataSet(design = formula_obj) %>%
            ## fit model
            DESeq -> dds
    })

## save deseq object
dir.create(output_deseq, showWarnings = FALSE)
dds %>%
    imap(~ save(.x, file=paste0(output_deseq, "/", .y, "_.Rdata")))

## collect gene information from tximeta
gene_info <- tibble(id = rowData(dds[[1]])$gene_id,
                    symbol = rowData(dds[[1]])$symbol,
                    description = rowData(dds[[1]])$description)

## collect results and add gene info
results <- dds %>%
    map(function(ds) {
        ds %>%
            results(tidy=TRUE) %>%
            rename(id=row) %>%
            inner_join(gene_info,
                       by="id") %>%
            tibble %>%
            select(symbol, everything(), description, id) %>%
            arrange(padj)
    })
    

## write out results tables
results %>%
    imap(~ write_tsv(.x, file=paste0(output_deseq, "/", .y, ".tsv")))
    
