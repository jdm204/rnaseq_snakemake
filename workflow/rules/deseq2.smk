rule pathway_analysis:
    input: "results/deseq2/results_{formula}.tsv",
    output: directory("results/deseq2/pathway/{formula}/"),
    conda: "../envs/gage.yaml",
    log: "logs/pathway_analysis/{formula}.log",
    script: "../scripts/pathway_analysis.R"

rule deseq2:
    input:
        quants=expand("results/salmon/{patient_sample}/",
                      patient_sample=PATIENT_SAMPLES),
        sample_table="config/units.tsv",
    output:
        results_table="results/deseq2/results_{formula}.tsv",
        deseq="results/deseq2/results_{formula}.rds",
    params:
        design="{formula}",
        q_prefix="results/salmon",
        q_suffix="quant.sf"
    log: "logs/deseq2/{formula}.log",
    conda: "../envs/deseq2.yaml",
    script: "../scripts/differential_expression.R"
