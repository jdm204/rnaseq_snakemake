rule tidy_impact:
    input: "results/haplotypecaller/annotated/{patient}_{sample}.tsv.gz",
    output: "results/haplotypecaller/annotated/filtered/{patient}_{sample}.tsv.gz",
    conda: "../envs/deseq2.yaml",
    script: "../scripts/filter_impact.R"

rule tidy_variant_table:
    input: "results/haplotypecaller/annotated/{patient}_{sample}.vcf.gz",
    output: "results/haplotypecaller/annotated/{patient}_{sample}.tsv.gz",
    params: "--vep-fields --lenient",
    container: "docker://registry.gitlab.com/jdm204/tidyvcf:latest",
    log: "logs/tidyvcf/{patient}_{sample}.log",
    shell:
        """
        (
        tidyvcf -i {input} --vep-fields | gzip > {output}
        ) &> {log}
        """
        
rule vep_annotate_small_variants:
    input:
        vcf="results/haplotypecaller/{patient}_{sample}.vcf.gz",
        vep_data="resources/ensembl-vep-data",
    output: "results/haplotypecaller/annotated/{patient}_{sample}.vcf.gz",
    conda: "../envs/vep.yaml",
    params:
        options="--vcf --everything --compress_output bgzip --cache --fork 4 --force_overwrite"
    threads: 8,
    shell:
        """
        vep \
        {params.options} \
        --dir {input.vep_data} \
        -i {input.vcf} \
        -o {output} 
        """

rule get_vep_data:
    output: directory("resources/ensembl-vep-data"),
    conda: "../envs/vep.yaml",
    shell:
        """
        vep_install \
        --AUTO cf \
        --NO_UPDATE \
        -s homo_sapiens \
        -y GRCh38 \
        --CACHE_VERSION 105 \
        -c {output} \
        --CONVERT
        """
        
