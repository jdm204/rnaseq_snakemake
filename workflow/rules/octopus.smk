rule octopus_concat_chromosome:
    input: expand("results/variants/octopus/{{patient}}_{{sample}}_{chr}.vcf.gz", chr=CHROMOSOMES),
    output: "results/variants/octopus/{patient}_{sample}.vcf.gz",
    conda: "../envs/bcftools.yaml",
    shell: "bcftools concat {input} -Oz -o {output} "

rule octopus_chromosome:
    input:
        ref="resources/ref/hg38/hg38.fa.gz",
        reffai="resources/ref/hg38/hg38.fa.fai",
        reads="results/STAR/{patient}_{sample}_Aligned.sortedByCoord.out.bam",
        readsidx="results/STAR/{patient}_{sample}_Aligned.sortedByCoord.out.bam.bai",
    output:
        vcf=temp("results/variants/octopus/{patient}_{sample}_{chr}.vcf.gz"),
        tbi=temp("results/variants/octopus/{patient}_{sample}_{chr}.vcf.gz.tbi"),
    threads: 32,
    conda: "../envs/octopus.yaml",
    params:
        annotations="AD ADP AF AFB",
    shell:
        """
        octopus \
        --threads {threads} \
        -R {input.ref} \
        --annotations {params.annotations} \
        --regions {wildcards.chr} \
        -I {input.reads} \
        -o {output.vcf} 
        """
