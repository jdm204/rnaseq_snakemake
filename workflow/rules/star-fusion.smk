def starfusion_trimmed_inputs(wc):
    units = uniq(UNITS.loc[wc.patient,:].loc[wc.sample,:]["unit"].tolist())
    return expand("results/trimmed/{patient}_{sample}_{unit}_val_{pair}.fq.gz",
                  patient=wc.patient, sample=wc.sample, unit=units, pair=[1,2])

def starfusion_inputs_string(wc):
    units = uniq(UNITS.loc[wc.patient,:].loc[wc.sample,:]["unit"].tolist())
    samplestring = "results/trimmed/{patient}_{sample}_{{unit}}_val_{{{{pair}}}}.fq.gz".format(patient=wc.patient, sample=wc.sample)
    unitstrings = expand(samplestring, unit=units)
    fwds = ",".join(map(lambda s: s.format(pair=1), unitstrings))
    revs = ",".join(map(lambda s: s.format(pair=2), unitstrings))
    return f"--left_fq {fwds} --right_fq {revs}"

rule star_fusion:
    input:
        fastq=starfusion_trimmed_inputs,
        resources="resources/ctat_genome_lib_build_dir/",
    output: directory("results/STAR-FUSION/{patient}_{sample}"),
    container: "docker://trinityctat/starfusion:1.11.0"
    params:
        input_string=starfusion_inputs_string,
    shell:
        """
        /usr/local/src/STAR-Fusion/STAR-Fusion \
        --genome_lib_dir {input.resources} \
        {params.input_string} \
        --output_dir {output} \
        --FusionInspector validate \
        --examine_coding_effect 
        """
        
rule star_fusion_resources:
    output:
        dir=directory("resources/ctat_genome_lib_build_dir/"),
        tar=temp("resources/STAR-FUSION.tar.gz"),
    params:
        url="https://data.broadinstitute.org/Trinity/CTAT_RESOURCE_LIB/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play.tar.gz",
    shell:
        """
        wget -c {params.url} -O {output.tar} && \
        tar -xf {output.tar} -C "resources/" --strip-components 1
        """
