rule debug_haplotypecaller:
    input:
        ref="resources/ref/hg38/hg38.fa.gz",
        bam="results/STAR/bqsr/{patient}_{sample}.bam",
    output:
        vcf="results/haplotypecaller/debug/{patient}_{sample}_{region}.vcf.gz",
        bam="results/haplotypecaller/debug/{patient}_{sample}_{region}_out.bam",
    log: "logs/debug_haplotypecaller/{patient}_{sample}_{region}.log",
    conda: "../envs/gatk.yaml",
    priority: 5,
    shell:
        """
        (
        gatk HaplotypeCaller \
        -R {input.ref} \
        -I {input.bam} \
        -O {output.vcf} \
        -L {wildcards.region} \
        --standard-min-confidence-threshold-for-calling 20 \
        --dont-use-soft-clipped-bases \
        --debug-assembly \
        -bamout {output.bam} \
        ) 2> {log}
        """

rule gatk_haplotypecaller:
    input:
        ref="resources/ref/hg38/hg38.fa.gz",
        bam="results/STAR/bqsr/{patient}_{sample}.bam",
    output:
        vcf=temp("results/haplotypecaller/{patient}_{sample}.vcf.gz"),
        bam="results/haplotypecaller/{patient}_{sample}_out.bam",
    conda: "../envs/gatk.yaml",
    priority: 5,
    shell:
        """
        gatk HaplotypeCaller \
        -R {input.ref} \
        -I {input.bam} \
        -O {output.vcf} \
        --standard-min-confidence-threshold-for-calling 20 \
        --dont-use-soft-clipped-bases \
        -bamout {output.bam}
        """

rule gatk_bqsr_2:
    input:
        bam="results/STAR/splitncigar/{patient}_{sample}.bam",
        ref="resources/ref/hg38/hg38.fa.gz",
        recal_table="results/STAR/base_recalibration/{patient}_{sample}.txt",
    output: temp("results/STAR/bqsr/{patient}_{sample}.bam"),
    conda: "../envs/gatk.yaml",
    priority: 4,
    shell:
        """
        gatk ApplyBQSR \
        -R {input.ref} \
        -I {input.bam} \
        -O {output} \
        --bqsr-recal-file {input.recal_table} 
        """

rule gatk_analysecovariates:
    input: "results/STAR/base_recalibration/{patient}_{sample}.txt",
    output: "results/STAR/base_recalibration/{patient}_{sample}.pdf",
    conda: "../envs/gatk.yaml",
    shell:
        """
         gatk AnalyzeCovariates \
        -bqsr {input} \
        -plots {output}
        """
        
rule gatk_bqsr_1:
    input:
        bam="results/STAR/splitncigar/{patient}_{sample}.bam",
        ref="resources/ref/hg38/hg38.fa.gz",
        refdict="resources/ref/hg38/hg38.dict",
        known_variation=multiext("resources/ref/hg38/dbsnp155", ".vcf.gz", ".vcf.gz.tbi"),
    output: "results/STAR/base_recalibration/{patient}_{sample}.txt",
    conda: "../envs/gatk.yaml",
    priority: 3,
    shell:
        """
        gatk BaseRecalibrator -I {input.bam} -R {input.ref} -O {output} \
        --known-sites {input.known_variation[0]}
        """
    
rule gatk_splitcigar:
    input:
        ref="resources/ref/hg38/hg38.fa.gz",
        reffai="resources/ref/hg38/hg38.fa.gz.fai",
        bam="results/STAR/deduplicated/{patient}_{sample}.bam",
    output: temp("results/STAR/splitncigar/{patient}_{sample}.bam"),
    conda: "../envs/gatk.yaml",
    priority: 2,
    shell:
        """
        gatk SplitNCigarReads \
        -R {input.ref} \
        -I {input.bam} \
        -O {output}
        """

rule dedup:
    input: "results/STAR/{patient}_{sample}_Aligned.sortedByCoord.out.bam",
    output:
        metrics="results/qc/duplicates/{patient}_{sample}.txt",
        bam=temp("results/STAR/deduplicated/{patient}_{sample}.bam"),
    conda: "../envs/gatk.yaml",
    shell: "gatk MarkDuplicates -I {input} -M {output.metrics} -O {output.bam}"

