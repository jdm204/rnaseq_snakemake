import os.path

rule trim_galore_pe:
    """
    Trim_Galore! A cutadapt wrapper for trimming adapters and poor quality sequences.
    this rule also outputs a trimming report, but trim_galore refuses to respect the
    specified filename for it (--basename) so it's not a snakemake output
    """
    input: patient_unit_fastqs,
    output:
        temp("results/trimmed/{patient}_{sample}_{unit}_val_1.fq.gz"),
        temp("results/trimmed/{patient}_{sample}_{unit}_val_2.fq.gz"),
    params:
        extra="-q 10 --basename {patient}_{sample}_{unit}",
        dir="results/trimmed/",
    log:
        "logs/trim_galore/{patient}_{sample}_{unit}.log",
    conda: "../envs/trim-galore.yaml",
    shell:
        """
        (trim_galore \
        {params.extra} \
        --paired \
        -o {params.dir} \
        {input}) \
        &> {log}
        """
