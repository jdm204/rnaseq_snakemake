rule salmon_mapping:
    input:
        read1s=salmon_library_fastqs_read1,
        read2s=salmon_library_fastqs_read2,
        index="resources/salmon_sa_index/default",
    output: directory("results/salmon/{patient}_{sample}"),
    conda: "../envs/salmon.yaml",
    params:
        main="-l A --validateMappings --gcBias --seqBias",
    log: "logs/salmon_mapping/{patient}_{sample}.log",
    shell:
        """
        (
        salmon quant \
        {params.main} \
        -i {input.index} \
        -1 {input.read1s} -2 {input.read2s} \
        -o {output} \
        ) &> {log}
        """
     
rule get_salmon_index:
    output: directory("resources/salmon_sa_index/default/"),
    params:
        prefix="resources/salmon_sa_index",
        url="http://refgenomes.databio.org/v3/assets/archive/2230c535660fb4774114bfa966a62f823fdb6d21acf138d4/salmon_sa_index?tag=default",
    log: "logs/get_salmon_index.log",
    shell:
        """
        (
        wget \
        {params.url} \
        -O {params.prefix}.tar.gz && \
        mkdir -p {params.prefix} && \
        tar -xf {params.prefix}.tar.gz --directory {params.prefix} \
        ) &> {log}
        """
