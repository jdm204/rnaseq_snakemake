rule fasta_dict:
    input: "{somepath}/{somefasta}.fa.gz",
    output: "{somepath}/{somefasta}.dict",
    conda: "../envs/gatk.yaml",
    shell: "gatk CreateSequenceDictionary -R {input} "

rule fasta_gz_index:
    input: "{somepath}/{somefasta}.fa.gz",
    output: multiext("{somepath}/{somefasta}.fa.gz", ".fai", ".gzi"),
    conda: "../envs/samtools.yaml",
    shell: "samtools faidx {input}"

rule decompress_fasta:
    input: "{somepath}/{somefasta}.fa.gz",
    output: "{somepath}/{somefasta}.fa",
    conda: "../envs/samtools.yaml",
    shell: "bgzip -dc {input} > {output}"      

rule fasta_index:
    input: "{somepath}/{somefasta}.fa",
    output: "{somepath}/{somefasta}.fa.fai",
    conda: "../envs/samtools.yaml",
    shell: "samtools faidx {input}"           

rule bam_index:
    input: "{path}/{filename}.bam",
    output: "{path}/{filename}.bam.bai",
    conda: "../envs/samtools.yaml",
    shell: "samtools index {input}"

rule tabix_index:
    input: "{path}/{filename}.vcf.gz",
    output: "{path}/{filename}.vcf.gz.tbi",
    conda: "../envs/samtools.yaml",
    shell: "tabix {input}"

rule get_hg38_reference:
    output: "resources/ref/hg38/hg38.fa.gz"
    params:
        url="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.40_GRCh38.p14/GRCh38_major_release_seqs_for_alignment_pipelines/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.gz"
    conda: "../envs/curl.yaml"
    shell:
        "curl -L {params.url} | bgzip -dc | bgzip -c > {output} "           

rule bgzip_vcf:
    input: "{path}/{filename}.vcf",
    output: "{path}/{filename}.vcf.gz",
    conda: "../envs/samtools.yaml",
    shell: "bgzip {input}"
        
rule get_hg38_dbsnp:
    output:
        vcf="resources/ref/hg38/dbsnp155.vcf",
    params:
        vcf="gs://gcp-public-data--broad-references/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf",
    conda: "../envs/gsutil.yaml",
    shell: "gsutil cp {params.vcf} {output.vcf}"        
