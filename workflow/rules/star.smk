rule compress_star_unmapped:
    input: "results/STAR/{patient}_{sample}_Unmapped.out.mate{read}",
    output: "results/STAR/{patient}_{sample}_Unmapped_{read}.fq.gz",
    conda: "../envs/samtools.yaml",
    priority: 1,
    shell: "bgzip -c {input} > {output}"

rule get_star_index:
    output: directory("resources/STAR"),
    params: "http://refgenomes.databio.org/v3/assets/archive/2230c535660fb4774114bfa966a62f823fdb6d21acf138d4/star_index?tag=default",
    shell:
        """
        wget {params} -O {output}.tar.gz &&
        mkdir {output} &&
        tar -xf {output}.tar.gz -C {output} --strip-components 1
        """

def star_trimmed_inputs(wc):
    units = uniq(UNITS.loc[wc.patient,:].loc[wc.sample,:]["unit"].tolist())
    return expand("results/trimmed/{patient}_{sample}_{unit}_val_{pair}.fq.gz",
                  patient=wc.patient, sample=wc.sample, unit=units, pair=[1,2])

def star_inputs_string(wc):
    units = uniq(UNITS.loc[wc.patient,:].loc[wc.sample,:]["unit"].tolist())
    samplestring = "results/trimmed/{patient}_{sample}_{{unit}}_val_{{{{pair}}}}.fq.gz".format(patient=wc.patient, sample=wc.sample)
    unitstrings = expand(samplestring, unit=units)
    fwds = ",".join(map(lambda s: s.format(pair=1), unitstrings))
    revs = ",".join(map(lambda s: s.format(pair=2), unitstrings))
    return f"{fwds} {revs}"

def star_RG_string(wc):
    units = uniq(UNITS.loc[wc.patient,:].loc[wc.sample,:]["unit"].tolist())
    line = "ID:{pt}_{sm} LB:{{unit}} SM:{pt}_{sm} PL:ILLUMINA".format(
        pt=wc.patient, sm=wc.sample)
    return " , ".join(expand(line, unit=units))

rule star:
    input:
        fastq=star_trimmed_inputs,
        star_idx="resources/STAR",
    output:
        bam="results/STAR/{patient}_{sample}_Aligned.sortedByCoord.out.bam",
        unmapped=temp(expand("results/STAR/{{patient}}_{{sample}}_Unmapped.out.mate{read}", read=[1,2])),
    params:
        out_prefix="results/STAR/{patient}_{sample}_",
        inputline=star_inputs_string,
        RGline=star_RG_string,
    conda: "../envs/STAR.yaml",
    threads: 8,
    shell:
        """
        STAR \
        --genomeDir {input.star_idx} \
        --runThreadN {threads} \
        --readFilesIn {params.inputline} \
        --outFileNamePrefix {params.out_prefix} \
        --chimSegmentMin 20 \
        --chimMultimapNmax 2 \
        --chimOutType Junctions \
        --outSAMtype BAM SortedByCoordinate \
        --outSAMattributes Standard \
        --readFilesCommand zcat \
        --twopassMode Basic \
        --outReadsUnmapped Fastx \
        --outSAMattrRGline {params.RGline}
        """
