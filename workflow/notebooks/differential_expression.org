#+title: Differential Expression Analysis
#+author: Jamie D Matthews

* Setup

Load packages.

#+begin_src R :session
library(tidyverse)
library(DESeq2)
#+end_src

Load data.

#+begin_src R :session
dds <- readRDS("../../results/deseq2/results_patient_+_condition.rds")
#+end_src
