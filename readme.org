#+title:  Turner Lab Bulk RNASeq Analysis Pipeline 🚀
#+author: Jamie D Matthews
#+date:  <2022-08-03 Wed>

The pipeline is implemented with Snakemake 🐍.

It is intended to output gene/transcript count tables, gene fusion predictions, and small variants.

In the future, automatic differential expression testing from a metadata table may be added.

* Config

Input FASTQ files should be specified in =config/units.tsv= (relative to the pipeline directory, ie =config= should be on the same directory level as =workflow=).

The format of this (tab-separated) table is as follows:

| column name | purpose                                    |
|-------------+--------------------------------------------|
| patient     | differentiate between organisms/cell lines |
| sample      | isolate of organism                        |
| pair        | read pair of FASTQ file (1/2)              |
| fastq       | path to corresponding FASTQ file           |

* Workflow

[[file:dag.png]]




